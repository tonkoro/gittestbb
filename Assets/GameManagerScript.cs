﻿using UnityEngine;
using System.Collections;

public class GameManagerScript : MonoBehaviour
{

    public int Coins = 0; //取ったコイン数を保持

    private Rect coinLabelRect = new Rect(10, 10, 400, 300);
    private GUIStyle coinLabelStyle;
    // Use this for initialization 
    void Start()
    {
        coinLabelStyle = new GUIStyle();
        coinLabelStyle.fontSize = 30;
        coinLabelStyle.normal.textColor = Color.white;
    }
    // Update is called once per frame 
    void Update()
    {
    }
    // GUI 
    void OnGUI()
    {
        GUI.Label(coinLabelRect, "Coins:" + Coins, coinLabelStyle);
    }
}
