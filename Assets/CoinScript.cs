﻿using UnityEngine;
using System.Collections;


public class CoinScript : MonoBehaviour {
    // パブリックなメンバ変数 
    public int RotateSpeed = 10;
    // Use this for initialization 
    void Start () {
    }
    // Update is called once per frame 
    void Update () {
        // コインを回転させる
        transform.Rotate(Vector3.up * Time.deltaTime * RotateSpeed, Space.World);
    }
}