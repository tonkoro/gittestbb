﻿using UnityEngine;
using System.Collections;

public class PlayerScript : MonoBehaviour
{
    // パブリックなメンバ変数
    public int Speed = 10;// 右向きのスピード 
    public int JumpPower = 50;// ジャンプ力 

    // Use this for initialization 
    void Start()
    {
    }

    // Update is called once per frame 
    void Update()
    {
        // 常に右に移動 
        transform.Translate(Vector3.right * Time.deltaTime * Speed);
        // ジャンプの実装 21 
        if (Input.GetButton("Jump"))
        {
            // コライダーよりPlayerと床の距離を計算 
            float sizeY = gameObject.GetComponent<BoxCollider>().size.y;
            // レイキャストで接地の判定 
            if (Physics.Raycast(transform.position, Vector3.down, sizeY))
            {
                // Playerに上向きの力を与える 
                GetComponent<Rigidbody>().AddForce(Vector3.up * JumpPower);
            }
        }
    }
    void OnCollisionEnter(Collision collision)
    {
        switch (collision.gameObject.name)
        {
            case "Goal":
                Time.timeScale = 0;
                break;
            case "Enemy":
                Time.timeScale = 0;
                break;
        }
    }
    void OnTriggerEnter(Collider other)
    {
        switch (other.gameObject.name) {
            case "Coin":
                // Coinの取得 
                GameManagerScript gameManagerScript = GameObject.Find("GameManager").GetComponent<GameManagerScript>();
                gameManagerScript.Coins++;
                // Coinを消す 
                Destroy(other.gameObject); 
                break;
        } 
        } 
}