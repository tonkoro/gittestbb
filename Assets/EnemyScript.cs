﻿using UnityEngine;
using System.Collections;
public class EnemyScript : MonoBehaviour
{
    // パブリックなメンバ変数 
    public int Speed = 5;
    // プライベートなメンバ変数 
    private float startDistance = 10.0f;// Playerとの距離がこの値以下になった時にEnemyを動かし始める 
    private bool isStarted = false;// Enemyが動き始めたかどうかのフラグ 
    private GameObject playerObj; // Playerオブジェクト 
                                 
    // Use this for initialization 
    void Start()
    {
        // Playerオブジェクトの参照を変数に格納 
        playerObj = GameObject.Find("Player");
    }
    // Update is called once per frame 
    void Update()
    {
        if (isStarted)
        {
            // 左に移動 
            transform.Translate(Vector3.left * Time.deltaTime * Speed);
        }
        else {
            if (transform.position.x - playerObj.transform.position.x < startDistance)
            {
                // Playerに近づいたらEnemyを動かし始める 
                isStarted = true;
            }
        }
    }
}